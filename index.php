<?php
    require_once ('template.php');

    $templateHtml =<<<END_HTML
    <div>{my_var}</div>
    {if bool_var}
        True
    {else}
        False
    {/if}
END_HTML;

    $template = new Template($templateHtml);
    $template->assign('my_var', 'test');
    $template->assign('bool_var', true);
    //var_dump($template);
    echo $template->parse();


