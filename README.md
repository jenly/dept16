Необходимо разработать простой шаблонизатор, который бы поддерживал Переменные и блок if
Шаблон всегда корректный. 
Возможны вложенные if.
Возможны if с else и без.

Пример вызова шаблонизатора
$templateHtml =<<<END_HTML
<div>{my_var}</div>
{if bool_var}
    True
{else}
    False
{/if}
END_HTML;

$template = new Template($templateHtml);
$template->assign('my_var', 'test');
$template->assign('bool_var', true);

echo $template->parse();

Результирующий вывод, который должен получиться в результате запуска
<div>test</div>
True

===========

Написать функцию подсчета слов в строке.
Разрешено использовать только 2 встроенные PHP ф-ции: strlen и substr.