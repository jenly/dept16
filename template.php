<?php

class Template
{

    protected $template = '';
    protected $conditionalRegex = '';
    protected $conditionalElseRegex = '';
    protected $conditionalEndRegex = '';
    protected $values = [];

    function __construct($template)
    {
        $this->template = $template;
        $this->conditionalRegex = '/\{\s*(if)\s*((?:\()?(.*?)(?:\))?)\s*\}/ms';
        $this->conditionalElseRegex = '/\{\s*else\s*\}/ms';
        $this->conditionalEndRegex = '/\{\s*\/if\s*\}/ms';
    }

    public function assign($key, $value)
    {
        $this->values[$key] = $value;
    }

    function parse()
    {
        foreach ($this->values as $key => $value) {
            $tag = "{" . $key . "}";
            $this->template = str_replace($tag, $value, $this->template);
        }
        return self::parseConditionals();
    }

    public function parseConditionals()
    {
        preg_match_all($this->conditionalRegex, $this->template, $matches, PREG_SET_ORDER);

        $changeValues = [];
        foreach ($matches as $match) {
            $insertVal = ($this->values[$match[2]]) ? 'true' : 'false';
            $changeValues[] = str_replace($match[2], $insertVal, $match);
            $this->template = str_replace($match[0], '{' . $match[1] . ' ' . $insertVal . '}', $this->template);
        }
        $matches = $changeValues;

        foreach ($matches as $match) {

            $condition = $match[2];

            $conditional = '<?php ';
            $conditional .= $match[1] . ' (' . $condition . ')';
            $conditional .= ': ?>';
            $this->template = preg_replace('/' . preg_quote($match[0], '/') . '/m', addcslashes($conditional, '\\$'), $this->template, 1);

        }

        $this->template = preg_replace($this->conditionalElseRegex, '<?php else: ?>', $this->template);
        $this->template = preg_replace($this->conditionalEndRegex, '<?php endif; ?>', $this->template);

        $this->template = $this->parsePhp($this->template);
        return $this->template;
    }

    protected function parsePhp($text)
    {
        ob_start();
        $result = eval('?>' . $text . '<?php ');
        if ($result === false) {
            $output = 'You have a syntax error in your Lex tags. The offending code: ';
            throw new ParsingException($output . str_replace(array('?>', '<?php '), '', $text));
        }
        return ob_get_clean();
    }

}